module mod_diff

  use iso_fortran_env, only: int32, real32

  implicit none

  private
  public :: diff_upwind
  public :: diff_centered

contains

  pure function diff_upwind(x) result(dx)
    real(real32), intent(in) :: x(:)
    real(real32) :: dx(size(x))
    integer(int32) :: im
    im = size(x)
    dx(1) = x(1) - x(im)        ! Apply BC: Periodic
    dx(2:im) = x(2:im) - x(1:im-1)
  end function diff_upwind


  pure function diff_centered(x) result(dx)
    real(real32), intent(in) :: x(:)
    real(real32) :: dx(size(x))
    integer(int32) :: im
    im = size(x)
    dx(1) = dx(2) - dx(im)      ! Apply Left BC
    dx(im) = dx(1) - dx(im-1)   ! Apply Right BC
    dx(2:im-1) = dx(3:im) - dx(1:im-2)
    dx = 0.5 * dx
  end function diff_centered

end module mod_diff

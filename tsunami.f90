! Tsunami Simulator

program tsunami

  use iso_fortran_env, only: int32, real32
  use mod_diff, only: diff => diff_centered
  use mod_initial, only: set_gaussian

  implicit none

  integer(kind=int32) :: i, n
  integer(kind=int32), parameter :: grid_size = 100
  integer(kind=int32), parameter :: num_time_steps = 10000

  real(kind=real32), parameter :: dt = 0.01  ! time step     [s]
  real(kind=real32), parameter :: dx = 1     ! grid spacing  [m]
  real(kind=real32), parameter :: g  = 9.81  ! grav. accel.  [m s^-2]
  real(kind=real32), parameter :: hmean = 10 ! mean water height [m]

  integer(kind=int32), parameter :: icenter = 25
  real(kind=real32), parameter :: decay = 0.02

  real(kind=real32), dimension(grid_size) :: h, u


  ! Check Inputs
  if (grid_size <= 0) stop 'grid_size must be > 0'
  if (dt <= 0) stop 'time step, dt [s], must be > 0'
  if (dx <= 0) stop 'grid spacing, dx [m], must be > 0'

  ! Initialise Array
  call set_gaussian(h, icenter, decay)
  u = 0

  !
  ! Simulate
  !

  ! Print Initialised Solution Space to stdout
  print *, 0, h

  time_loop: do n = 1, num_time_steps
    u = u - dt * (u * diff(u) + g * diff(h)) / dx
    h = h - dt * diff(u * (hmean + h)) / dx
    print *, n, h
  end do time_loop

end program tsunami

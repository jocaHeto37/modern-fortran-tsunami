""" plot.py

    Reads output of tsunami and plots the results in an image file.

    """

__all__ = []
__version__ = "ch02"


import argparse
import os
import sys
from dataclasses import dataclass

import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 16})


@dataclass
class TsunamiData:
    """ Simple class for tsunami simulation data. """
    t: np.ndarray
    h: np.ndarray
    x: np.ndarray


def read_data(fname: str) -> type[TsunamiData]:
    """ Read data from file output by tsunamic simulator. """
    unicodeVar = "utf-16" if sys.platform == "win32" else "utf-8"

    with open(fname, mode="r", encoding=unicodeVar) as f:
        data = np.array([line.rstrip().split() for line in f.readlines()])

    return TsunamiData(
        t = data[:,0].astype(float),
        h = data[:,1:].astype(float),
        x = np.arange(1, data.shape[1]))


def draw_multipanel(
        fname: str,
        tsunami: type[TsunamiData],
        times: list[int] = [0],
        width: float = 10.0,
        sub_height: float = 0.24
    ) -> None:
    """ Plot solution space for each of the provided times. """
    n_plots = len(times)
    w = width
    h = n_plots * (sub_height * width) + 1

    fig = plt.figure(figsize=(w, h))
    axes = [plt.subplot2grid(
                (n_plots, 1),
                (row, 0),
                colspan=1,
                rowspan=1)
            for row in range(n_plots)]

    xlim = tsunami.x.max()

    for ax in axes:
        n = axes.index(ax)
        idx = (np.abs(tsunami.t - times[n])).argmin()
        ax.plot(tsunami.x, tsunami.h[idx, :], 'b-')
        ax.fill_between(tsunami.x, 0, tsunami.h[idx, :], color='b', alpha=0.4)
        ax.grid()
        ax.set_xlim(1, xlim)
        ax.set_ylim(0, 1)
        ax.set_ylabel('Height', fontsize=16)
        ax.set_xticks(np.array([0.25, 0.5, 0.75, 1.])*xlim)
        ax.set_yticks([0, 0.25, 0.5, 0.75, 1])
        ax.set_title(f"Time: {tsunami.t[idx]:03.1f}")

    for ax in axes[:-1]:
        ax.set_xticklabels([])
        ax.set_xlabel('', fontsize=16)

    axes[-1].set_xlabel('Spatial grid index')

    plt.savefig(fname)
    plt.close(fig)


def init_parser() -> type[argparse.ArgumentParser]:
    """ Initialise the cli parser """
    parser = argparse.ArgumentParser()

    parser.add_argument(
        'input_file',
        help='text file output by the tsunami simulator (chapter 2 version)')

    parser.add_argument(
        "-n",
        "--name",
        dest = "output_file",
        metavar = "NAME",
        action = "store",
        type = str,
        help = "Name of image file. Defaults to same name as input file\n"
            "but with the '.svg' extension.")

    parser.add_argument(
        "-t",
        "--times",
        dest = "times",
        metavar = "TIMES",
        action = "append",
        type = float,
        nargs = "+",
        help = "List of times to show in subplots.")

    return parser


def main() -> None:
    """ Main function. Acting as script entry point. """
    parser = init_parser()
    args = parser.parse_args()
    input_file = args.input_file

    if not os.path.exists(args.input_file):
        print(f"The file {args.input_file} does not exits.")
        sys.exit(1)

    try:
        fname = args.output_file
    except ValueError:
        fname = os.path.splitext(args.input_file)[0] + ".svg"

    try:
        times = args.times[0]
    except TypeError:
        times = [0]

    draw_multipanel(
        fname = fname,
        tsunami = read_data(args.input_file),
        times = times)


if __name__ == "__main__":
    main()

# Tsunami Simulator

Running project for the book: "Modern Fortran - Building Efficient Parallel Applications"

## Development Setup

### Void Linux

Install tools:

`sudo xbps-install git make cmake`

Install Fortran compiler:

`sudo xbps-install gcc-fortran`

Check...

`gfortran --help`


Install OpenMPI:

`sudo xbps-install openmpi openmpi-devel`

Check...

`mpif90`
> Gives error, but command should be found


Install OpenCoarrays:

`git clone --branch 2.9.0 https://github.com/sourceryinstitute/OpenCoarrays.git`

`cd OpenCoarrays`

`mkdir build`

`cd build`

`FC=gfortran CC=gcc cmake ..`

`make`

`sudo make install`

Check...

`caf --help`

`cafrun --help`


### Docker Container

[Dockerfile](https://github.com/modern-fortran/modern-fortran-docker/blob/master/Dockerfile)

`docker build . -t modern-fortran:latest`

`docker run -it modern-fortran:latest /bin/bash`


### Python Environment

Ensure python is installed along with any additional dependencies for python's `venv` module to work.
(Ubuntu usually has this packaged seperately.)

Create environment:

`python -m venv venv --prompt="tsunami" --upgrade-deps`

Activate:

`. venv/bin/activate`

Install necessary python modules:

`python -m pip install -r requirements.txt`

This will include `numpy` and `matplotlib` at least.


## Usage

Compile Code:

`gfortran tsunami.f90 -o tsunami`

Run and save output:

`./tsunami > output`

Produce graphs from output (python environment needs to be active):

`python plot.py output --name output.svg --times 0 100 200`

Specify the times ([s]) to include in the plot as needed.


## To Do
- Learn how to use `make` to handle the full dependecy tree.
    - See tsunami github for inspiration.
-
